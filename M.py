class User:
    def __init__(self, login: str):
        self.login = login

    def get_login(self):
        return self.login

    def show_product(self, name: str):
        return "Описание товара " + str(name)


class Client(User):
    def buy(self, name: str):
        return "Товар " + str(name) + " куплен"


class Manager(User):
    def create_product(self, name: str):
        return "Новый товар " + str(name) + " успешно создан"

    def update_product(self, name: str):
        return "Товар " + str(name) + " успешно обновлен"

    def delete_product(self, name: str):
        return "Товар " + str(name) + " успешно удален"


class SuperManager(Manager):
    def set_discount(self, value: int, client: Client):
        return "Клиенту " + str(client.login) + " начислена скидка в размере " + str(value) + "%"


class Admin(User):
    def create_user(self, name, type):
        if type == "client":
            return Client(name)
        elif type == "manager":
            return Manager(name)
        elif type == "supermanager":
            return SuperManager(name)
        elif type == "admin":
            return Admin(name)

    def delete_user(self, name: str):
        return "Пользователь " + str(name) + " успешно удален"